include config.mk

BIN = lsgrp
SRC = lsgrp.c
OBJ = $(SRC:.c=.o)
BINDIR = $(DESTDIR)$(PREFIX)/bin

.PHONY: all clean install

all: $(BIN)

$(BIN): $(OBJ)
	$(CC) $(LDFLAGS) -o $@ $^

.SUFFIXES: .c .o
.c.o:
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) -c -o $@ $<

clean:
	rm -f $(OBJ) $(BIN)

install:
	install -d $(BINDIR)
	install -m 755 $(BIN) $(BINDIR)
	install -d $(MANPREFIX)/man1
	install -m 644 lsgrp.1 $(MANPREFIX)/man1
	gzip -f $(MANPREFIX)/man1/lsgrp.1
	install -d $(DESTDIR)$(PREFIX)/doc/lsgrp
	install -m 644 COPYING $(DESTDIR)$(PREFIX)/doc/lsgrp
