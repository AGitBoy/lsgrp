# config.mk
CC = cc

PREFIX    = /usr/local
MANPREFIX = $(DESTDIR)$(PREFIX)/share/man

CPPFLAGS  = -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=700
CFLAGS    = -std=c99 -pedantic -Wall -O1
LDFLAGS   =
