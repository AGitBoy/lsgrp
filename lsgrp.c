/*
 * lsgrp.c: lists system groups
 *
 * Copyright (C) 2022 Avalon Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <grp.h>
#include <errno.h>

int
main(int argc, char **argv)
{
	struct group *g;
	int sflag = 0;
	int uflag = 0;
	int opt;

	while ((opt = getopt(argc, argv, ":su")) != -1) {
		switch(opt) {
			case 's':
				sflag = 1;
				break;
			case 'u':
				uflag = 1;
				break;
			case '?':
				fprintf(stderr, "usage: %s [-su]\n", argv[0]);
				break;
		}
	}
	
	if (!sflag) {
		if (uflag)
			printf("%-10s %-20s %s\n", "ID", "GROUP", "USERS");
		else
			printf("%-10s %s\n", "ID", "GROUP");
	}

	errno = 0;
	while ((g = getgrent())) {
		if (errno) {
			fprintf(stderr, "%s: getgrent: %s\n", argv[0], strerror(errno));
			return 1;
		}

		if (uflag) {
			printf("%-10i %-20s\t", g->gr_gid, g->gr_name);

			while (*(g->gr_mem) != NULL) {
				printf("%s", *(g->gr_mem));

				if (*++(g->gr_mem) != NULL)
					putchar(',');
			}

			putchar('\n');
		} else {
			printf("%-10i %s\n", g->gr_gid, g->gr_name);
		}
	}

	endgrent();

	return 0;
}
